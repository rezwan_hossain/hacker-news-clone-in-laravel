<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
         'user_id' => factory(User::class)->create(),
         'title' => $faker->sentence,
         'url' => $faker->url,
         'text' => $faker->paragraph,
         'points' => 0,
    ];
});
