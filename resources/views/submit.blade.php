@extends('layouts.layout')

@section('content')
<td>
	<table>
		<tbody>
			<tr>
				<td>
					<form method="POST" action="/posts">
						@csrf
						<tr>
							<td>title</td>
							<td>
								<input type="text" name="title" size = "50">
							</td>
						</tr>
						<tr>
							<td>url</td>
							<td>
								<input type="text" name="url" size = "50">
							</td>
						</tr>
						<tr>
							<td>text</td>
							<td>
								<textarea name="text" rows="4" cols="48"></textarea>
							</td>
						</tr>
						<tr>
							<td></td>
							<td><input type="submit" value="submit"></td>
						</tr>
						<tr></tr>
					</form>
				</td>
			</tr>
		</tbody>
	</table>
</td>	
@endsection