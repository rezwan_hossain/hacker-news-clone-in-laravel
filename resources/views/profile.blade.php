@extends('layouts.layout')

@section('content')
	<td>
		<table>
			<tbody>
				<tr>
					<td>user:</td>
					<td>{{$user->name}}</td>
				</tr>
				<tr>
					<td>created:</td>
					<td>{{$user->created_at->diffForHumans()}}</td>
				</tr>
			</tbody>
		</table>
	</td>
@endsection