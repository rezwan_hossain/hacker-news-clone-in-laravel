@extends('layouts.layout')

@section('content')
<td>
	<table>
		<tbody>
			<tr>
				<td align="right" valign="top">
					<span></span>
				</td>
				<td valign="top">
					<center>
						<form method="POST" action="/votes/{{$post->id}}">
							@csrf
							@method('PATCH')
							<button type="submit" class="votearrow"></button>
						</form>
					</center>
				</td>
				<td>
					<span>
						<a href="">{{ $post->title }}</a>
						<a class="title_link" href="https://{{$post->url}}" style="margin-left: 20px">{{$post->url}}</a>
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td class="subtext">
					<span>{{$post->points}} points</span>
					<a href="/user/{{$post->user->id}}">{{$post->user->name}}</a>
					<span>
						<a href=""> {{$post->created_at->diffForHumans()}} </a>
						<a href=""> {{$post->comments->count() + $post->comments->count() }} comments </a>
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td class="description" style="padding-top: 10px;padding-left: 8px;">
					<span>{{$post->text}}</span>
				</td>
			</tr>
			<tr style="height: 15px;"></tr>
			<tr>
				<td colspan="2"></td>
				<td>
					<form method="POST" action="/comments">
						@csrf
						<input type="hidden" name="post_id" value="{{$post->id}}">
						<textarea name="comment" rows="6" cols="60"></textarea>
						<br>
						<br>
						<input type="submit" value="add comment">
					</form>
				</td>
			</tr>
		</tbody>
	</table>
	<br>
	<br>
	<br>
	<table>
		<tbody>
			@foreach($post->comments as $comment)
				<tr>
					<td>
						<table>
							<tbody>
								<tr>
									<td>
										<div style="margin-top:2px; margin-bottom:-15px;">
											<span class="userName">
												<a href="/user/{{$comment->user_id}}"> {{$comment->name}} </a>
												<span>{{$comment->created_at->diffForHumans()}}</span>
											</span>
										</div>
										<br>
										<div>
											<span class="comment">{{$comment->comment}}</span>
											<div>
												<button class="replyButton">reply</button>
												<div style="padding: 5px 30px; display: none">
													<form method="POST" action="/replies">
														@csrf
														<input type="hidden" name="post_id" value="{{$post->id}}">
														<input type="hidden" name="comment_id" value="{{$comment->id}}">
														<textarea name="reply" rows="5" cols="40"></textarea>
														<br>
														<br>
														<input type="submit" value="reply">
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										@foreach($comment->replies as $reply)
											<div style="padding:10px 40px;">
												<span class="replyName">
													<a href="/user/{{$reply->user_id}}">{{$reply->name}}</a>
												</span>
												<span class="replyNameTime">{{$reply->created_at->diffForHumans()}}</span>
												<br>
												<span class="reply">{{$reply->reply}}</span>
											</div>
										@endforeach
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</td>
@endsection