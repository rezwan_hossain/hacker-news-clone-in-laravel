<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
</head>
<body>
	<br>
	<br>
	<br>
	<b>Login</b>
	<br>
	<br>
	<form method="POST" action="{{ route('login') }}">
		@csrf
		<table>
			<tbody>
				<tr>
					<td>Email : </td>
					<td>
						<input type="email" name="email" size="20">
					</td>
				</tr>
				<tr>
					<td>Password : </td>
					<td>
						<input type="password" name="password">
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<input type="submit" name="login">
	</form>
	<br>
	<br>
	<br>
	<b>Create Account</b>
	<br>
	<br>
	<form method="POST" action="{{ route('register') }}">
		@csrf
		<table>
			<tbody>
				<tr>
					<td>Name : </td>
					<td>
						<input type="text" name="name" size="20">
					</td>
				</tr>
				<tr>
					<td>Email : </td>
					<td>
						<input type="email" name="email" size="20">
					</td>
				</tr>
				<tr>
					<td>Password : </td>
					<td>
						<input type="password" name="password">
					</td>
				</tr>
				<tr>
					<td>Confirm Password : </td>
					<td>
						<input type="password" name="password_confirmation">
					</td>
				</tr>
			</tbody>
		</table>
		<br>
		<input type="submit" value="Create Account">
	</form>
</body>
</html>