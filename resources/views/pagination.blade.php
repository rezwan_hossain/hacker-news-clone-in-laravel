@if($paginator->hasPages())
    @if($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" style="color: blue;">More</a>
    @endif
@endif


{{-- if we want to add Prev and Next page --}}
{{-- @if($paginator->hasPages())
    <ul class="pagination">
        @if($paginator->onFirstPage())
            <li class="disabled"><span>{{ __('Prev') }}</span></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">{{ __('Prev') }}</a></li>
        @endif

        @if ($paginator->hasMorePages())
                    <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">{{ __('Next') }}</a></li>
                @else
                    <li class="disabled"><span>{{ __('Next') }}</span></li>
                @endif
    </ul>
@endif --}}
