@extends('layouts.layout')

@section('content')
<td>
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            
            {{-- Paginator Instance Methods = firstItem() --}}
            {{-- @foreach($posts as $key => $post) --}}
            @foreach($posts as  $post)
                <tr>
                    <td align="right" valign="top">
                        <span>{{ $posts->firstItem() -1 + $loop->iteration}}</span>
                        {{-- <span>{{ $posts->firstItem() + $key }}</span> --}}
                        {{-- <span>{{ ($posts->currentPage() - 1) * $posts->perPage() + $loop->iteration }}</span> --}}
                    </td>
                    <td valign="top">
                        <center>
                            <form method="POST" action="/votes/{{$post->id}}">
                                @csrf
                                @method('PATCH')
                                <button type="submit" class="votearrow"></button>
                            </form>
                        </center>
                    </td>
                    <td>
                        <a href="/posts/{{ $post->id }}">{{$post->title}} </a>  
                    </td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td class="subtext"> 
                        <span>{{$post->points}} points</span> by 
                        <a href="/user/{{$post->user->id}}">{{$post->user->name}}</a>
                        <span>
                        <a href="#"> {{$post->created_at->diffForHumans()}} </a>
                        <a href="/posts/{{ $post->id }}">{{$post->comments->count()}} comments</a>
                        </span>
                    </td>
                </tr>
                <tr style="height: 5px;"></tr>
            @endforeach
        </tbody>
    </table>
    <span>{{$posts->links('pagination')}}</span>
</td>
@endsection