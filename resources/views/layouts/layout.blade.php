<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@300&display=swap" rel="stylesheet">
        <title>News</title>
        <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <center>
            <table id="" border="0" cellpadding="0" cellspacing="0" width="85%" bgcolor="#f6f6ef">
                <tbody>
                    <tr>
                        <td bgcolor="#ff6600">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding: 2px">
                                <tbody>
                                    <tr>
                                        <td style="width: 10px;padding-right: 4px">
                                            <a href="/">
                                            <img src="https://news.ycombinator.com/y18.gif" width="18"height="18" style="border: 1px white solid;">
                                            </a>
                                        </td>
                                        <td style="line-height: 12px; height: 10px;">
                                            <span class="pagetop">
                                            <b class="hnname">
                                            <a href="news" >Hacker News</a>
                                            </b>
                                            <a href="newest">new</a> | 
                                            <a href="front">past</a> | 
                                            <a href="newcomments">comments</a> | 
                                            <a href="ask">ask</a> | 
                                            <a href="show">show</a> | 
                                            <a href="jobs">jobs</a> | 
                                            <a href="/posts/submit">submit</a>
                                            </span>
                                        </td>
                                        <td style="text-align: right;padding-right: 4px;">
                                            @guest
                                            <a href="{{ route('login') }}">login</a>
                                            @else(Route::has('login'))
                                            <span>
                                                <a href="/user/{{Auth::user()->id}}">{{ Auth::user()->name }}</a> |
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                logout
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form>
                                            </span>
                                            @endguest
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 10px;"></tr>
                    <tr>
                        @yield('content')
                    </tr>
                </tbody>
            </table>
        </center>
        <script type="text/javascript">
            function toggleReply(event) {
                if(event.target && event.target.className == 'replyButton') {
                    var next = event.target.nextElementSibling;

                    if (next.style.display == 'none') {
                        next.style.display = 'block';
                    } else {
                        next.style.display = 'none';
                    }
                }
            }
            document.addEventListener('click', toggleReply)
        </script>
    </body>
</html>