<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PostController@index');

Auth::routes();

Route::get('/posts/submit','PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts/{id}', 'PostController@show');
Route::patch('/votes/{id}', 'PostController@vote');

Route::post('/comments', 'CommentController@store');
Route::post('/replies', 'ReplyController@store');

Route::get('/user/{id}', 'ProfileController@show');
