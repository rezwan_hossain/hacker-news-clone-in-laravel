<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Reply;

class ReplyController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['except' => ['index', 'show']]);
	}
    
    public function store()
    {
    	$reply = new Reply;
    	$reply->user_id = Auth::user()->id;
    	$reply->post_id = request('post_id');
    	$reply->comment_id = request('comment_id');
    	$reply->name = Auth::user()->name;
    	$reply->reply = request('reply');

    	$reply->save();
    	return redirect()->back();

    }
}
