<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class CommentController extends Controller
{
	public function __construct() {
	    $this->middleware('auth', ['except' => ['index', 'show']]);
	}

    public function store()
    {
    	$comment = new Comment;
    	$comment->user_id = Auth::user()->id;
    	$comment->post_id = request('post_id');
    	$comment->name = Auth::user()->name;
    	$comment->comment = request('comment');

    	$comment->save();
    	return redirect()->back();
    }
}
