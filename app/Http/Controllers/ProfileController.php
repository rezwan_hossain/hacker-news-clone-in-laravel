<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class ProfileController extends Controller
{
    public function show($id)
    {
    	$user = User::findOrFail($id);
    	return view('profile', compact('user'));
    }
}
