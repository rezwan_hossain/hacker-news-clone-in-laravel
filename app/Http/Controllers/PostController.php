<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['except' => ['index', 'show']]);
	}

   public function index()
   {
   		$posts = Post::latest('created_at')->simplepaginate(30);
   		return view('index', compact('posts'));
   }

   public function create()
   {
   	return view('submit');
   }

   public function show($id)
   {
   		$post = Post::findOrFail($id);

   		return view('show', compact('post'));
   }

   public function store()
   {
   		$post = new Post;
   		$post->user_id = Auth::user()->id;
   		$post->title = request('title');
   		$post->url = request('url');
   		$post->text = request('text');
   		$post->points = 0;

   		$post->save();

   		return redirect('/');
   }

   public function vote($id)
   {
      $post = Post::findOrFail($id);
      $post->increment('points');
      return redirect()->back();
   }
}
